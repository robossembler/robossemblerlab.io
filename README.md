# Документация проекта "Робосборщик"

## Внесение изменений

### В существующие страницы

Войти в директорию `docs/` и редактировать нужный документ:

`docs/doc-to-be-edited.md`

```markdown
---
id: page-needs-edit
title: This Doc Needs To Be Edited
---

Edit me...
```

## Создание новых страниц

### Добавление новых страниц в существующие директории

1. Создайте документ с расширением .md в директории `/docs`, например `docs/newly-created-doc.md`(id должен совпадать с названием файла без расширения .md):

```md
---
id: newly-created-doc
title: Заголовок страницы
---

Новый контент..
```

1. Создайте ссылку на новый документ через id в боковое меню (файл `sidebars.js`):

```javascript
// Добавить документ newly-created-doc в соответствующий раздел docs (в данном случае 'Робосборщик')
module.exports = {
  docs: [
    {
      type: 'category',
      label: 'Робосборщик',
      items: [
        'robossembler-overview',
        'plan',
        "newly-created-doc" // сюда добавляется название нового документа без .md
      ],
    },
  ],
};
```

Дополнительная информация по редактированию контента [here](https://docusaurus.io/docs/en/navigation)

Этот сайт создан с помощью [Docusaurus 2](https://v2.docusaurus.io/), a modern static website generator.

## Установка

```console
yarn install
```

## Запуск сервера для локальной разработки

```console
yarn start
```

Можно смотреть изменения в коде онлайн - перезапуск сервера не требуется

## Сборка

```console
yarn build
```

Команда генерирует статический контект в директории `build`. Контент потом можно разместить как html на хостинге.
