---
id: smerobotics
title: 'Проект SMErobotics'
---

*Оригинал: [SMErobotics - Smart Robots for Flexible Manufacturing, 2019](http://dx.doi.org/10.1109/MRA.2018.2879747)*

*Авторы: By Alexander Perzylo, Markus Rickert, Björn Kahl, Nikhil Somani, Christian Lehmann, Alexander Kuss, Stefan Profanter, Anders Billes Beck, Mathias Haage, Mikkel Rath Hansen, Malene Tofveson Nibe, Máximo A. Roa, Olof Sörnmo, Sven Gestegård Robertz, Ulrike Thomas, Germano Veiga, Elin Anna Topp, Ingmar Kessler, and Marinus Danzer*

https://www.smerobotics.org/

## Предпосылки

Классические индустриальные системы недостаточно гибки для применения в секторе SME (small & medium enterprise) - организациям в промышленности численностью от 250 человек. По статистике такие компании представляют 99% бизнеса в Европейском Союзе. Проект SMErobotics ставит целью стимулировать применение роботов в данном секторе экономики.

Сфера применения промышленных роботов в настоящее время - крупно-масштабные производства одинаковых изделий. В тоже время SME нуждаются в гибких системах, так как не обладают таким масштабом и вынуждены работать с большой номенклатурой товаров и часто подстраиваться под изменения на рынке. Также в малых компаниях нет IT-подразделений, которые необходимы для поддержки систем автоматизации и роботизированныхя ячеек. В тоже время они имеют в распоряжении очень квалифицированных рабочих и продукт-менеджеров. Таким образом, предприятиям SME требуются такие роботизированные системы, которые смогут обслуживать квалифицированные рабочие. Роботы будут обучаться их навыкам и извлекать выгоды от взаимодействия с ними.

Опросы малых предприятий в Дании показали, что в 89% предприятий численностью менее 34 сотрудников нет вообще никакой автоматизации.

Основные проблемы, препятствующие использованию промышленных роботов в производство:
1. Современные методы программирования роботов не подходят для частых изменений настраиваемых продуктов, производимых небольшими партиями.
2. Производственные процессы, ориентированные на инструменты, требуют инвестиций в замену инструментов, подходящих для роботов.
3. Классические роботизированные ячейки с ограждениями занимают больше места, чем сопоставимые рабочие места для ручного труда.
4. Формализация неявных производственных знаний в технические спецификации или программы роботов - сложная задача (ред. - см. Парадокс Поланьи)
5. Эксплуатация промышленных роботов сложна и требует экспертных знаний в области робототехники.
6. Лицам, принимающим решения в Швейцарии, не хватает опыта в области робототехники: они не могут должным образом оценить возможности роботизированных систем или спрогнозировать связанные с этим затраты.

По статистике 63.4% затрат на роботизацию ложится на обслуживание роботов, и только 27.7% на их покупку.

## Решения, предлагаемые SMERobotics

### Развитие технологий человеко-машинного взаимодействия (human-robot interation, HRI)
### Investment Decision Support Tool
Рамках проекта был разработан веб-сервис для оценки перспективности инвестиций в роботизацию - https://www.robotinvestment.eu/ (сейчас домен перенаправляет на google)
### Внедрение технологий управления знаниями
* Моделирование программно-аппаратных компонентов с учётом их взаимодействия друг с другом (прим.ред. - см. Семантическая интероперабельность)
* Модель описания технологий Product–Process–Resource (продукт-процесс-ресурс)
![](img/smerobotics-extended-product–process–resource-modeling.jpg)
Различные типы моделей создаются и (повторно) используются для интеграции в решениях промышленной автоматизации. Модели процессов повторно используют технологические модели, чтобы снизить затраты на параметризацию этапов процесса. Модели процессов относятся к свойствам продукта и ресурсам, используемым при производстве продукта.
* Явное выделение семантики. Отделение программного кода от знаний предметной области (прим.ред. - см. моделе-ориентированное проектирование). Применение технологий semantic web - OWL, графов знаний.
### Использование навыков, учитывающих неопределенность
Подход SMErobotics заключается в том, чтобы инкапсулировать управление неопределенностью в так называемые навыки, которые могут быть параметризованы и повторно использованы в различных приложениях. Это снижает затраты на программирование для таких задач и снижает требования к точности расположения деталей в рабочей ячейке. Навыки - это базовые операции, которые могут быть объединены для формирования
сложных задач. В зависимости от возможности повторного использования навыки, как правило, можно разделить на 1) универсально применимые навыки; 2) общие навыки для определенной области применения; 3) специальные навыки для ряда продуктов/одного продукта.
Универсальные навыки, такие как обработка, комплектация или размещение объектов (см. рисунок ниже), актуальны не только для сборки, но и для большинства других областей. Например, сложной и хорошо известной проблемой является операция сборки колышка в отверстии, когда колышек необходимо вставить в узкое отверстие с помощью управления усилием. После однократного применения навыка его можно повторно использовать для ряда колышков и отверстий, адаптировав параметры навыка.

*Пример спецификации навыка с обратными связями:*
![](img/skill-structure.png)
### Минимизация времени цикла, основанная на обучении (Learning-Based Cycle-Time Minimization)
Например, оптимизация траектории движения фрезы на станке
### Обнаружение аномалий и обработка ошибок
Предлагается использовать Байесовские сети и расширенные цепи Маркова, чтобы выявлять первопричины аномалий и ошибок. Иллюстрация ниже демонстрирует пример выявления вероятности причины отказа:
![](img/smerobotics-error-handling.jpg)
### Продукто-центричные инструкции для роботов (Product-Centric Instruction of Robots)
Параметры задач выводятся из модели(описания) продукта, а не задаются напрямую инструкциями для машины. Декларативный подход в противовес доминирующему сейчас императивному. 
### Продукто-ориентированная автоматическая генерация программ для сборки (Product-Driven Program Generation for Assembly)
В рамках проекта был разработан автоматический планировщик сборки (automatic assembly planner). Планировщик автоматически генерирует несколько взаимосвязей попарной разборки, которые в сочетании с информацией о
силах и связности приводят к созданию AND/OR-графа, описывающего возможные последовательности сборки. Тесная интеграция между планировщиками захвата (grasp planner) и движения (motion planner) гарантирует, что узлы могут быть схвачены и выполнены в рабочей ячейке робота. Планировщик захвата рассматривает информацию, касающуюся ограничений и возможных столкновений с окружающей средой, а также процесс сборки и само действие соединения при минимизации требуемой целевой функции, например, крутящего момента, оказываемого на захват. На основе этого процесса соответствующие узлы AND/OR-графа обрезаются, в результате чего получается последовательность допустимых шагов для получения желаемой сборки [19]. Чтобы эффективно анализировать доступное рабочее пространство для робота, мы дополнительно разработали представление возможностей движения. Возможные местоположения целей предварительно вычисляются в автономном режиме и эффективно сохраняются на карте возможностей [20]. В результате карта
может быть эффективно запрошен в ходе выполнения программы для определения достижимости поз конечного эффектора.

## Применение технологий в реальных задачах

Данные демонстрации были проведены для оценки новых технологий в проекте SMErobotics:

1. Сборка коробки передач/редуктора [видео](https://www.youtube.com/watch?v=B1Qu8Mt3WtQ)
2. Сборка защелки клапана (Tecnalia)
3. Клепки для сборки настраиваемые захваты (Kuka, Германия)
4. Сборка алюминиевых профилей (DLR, Оберпфаффенхофен) [видео](https://www.youtube.com/watch?v=2jYhdmk-pMg)
5. Сборка вариантов клапанов
6. Сборка преобразователей энергии с затягиванием винтов (DTI - Danfoss, Дания) [видео](https://www.youtube.com/watch?v=sgulAxn5-_o)
7. Сварка с автоматической коррекцией траектории движения (Fraunhofer IPA, Германия)
8. Интуитивное обучение для сварочных задач (INESC-TEC, Португалия)
9. Сооружение стены для деревянного дома

## Заключение

Внедрение интеллектуальных роботов с когнитивными способностями в роботизированные и автоматизированные системы является чрезвычайно сложной задачей и выходит за рамки одного проекта, особенно для гибких производственных условий, обычно используемых на малых и средних предприятиях. Внутри SMErobotics, мы определили основные проблемы, которые в настоящее время препятствуют использованию роботов на малых и средних предприятиях, и разработали несколько
решений, выходящих за рамки современного уровня, которые приводят к повышению производительности и качества. С помощью восьми различных демонстраторов мы показали, что эти решения актуальны и применимы во этих отраслях. 

Интеграция знаний на основе семантических моделей облегчает формальное описание соответствующих аспектов систем автоматизации. Совершенствование этих моделей для полного кодирования знаний о конкретной области и интеграция большего числа областей станут важной задачей в будущем. Способность справляться с неопределенностями на нескольких уровнях имеет важное значение в неструктурированных средах, подобных МСП. Навыки, учитывающие неопределенность, могут
обеспечить гибкость и адаптивность, необходимые для реализации процессов, которые ранее считались неосуществимыми для автоматизации. Использование парадигмы программирования, основанной на навыках, постепенно расширяется в
отрасли, о чем свидетельствует недавно опубликованный проект стандартизированных наборов навыков от немецкой Ассоциации машиностроительной промышленности (VDMA). Эти так называемые сопутствующие спецификации VDMA охватывают различные области, такие как робототехника или интегрированные сборочные решения. Наши первоначальные оценки
показали, что снижение сложности программирования промышленных роботов имеет важное значение для финансово жизнеспособного мелкосерийного производства. Более всесторонние оценки должны проводиться в будущее, чтобы гарантировать, что наши концепции подходят для более широкой целевой аудитории.

Отзывы конечных пользователей на выставках в Ганновере и Automatica в 2014 и 2016 годах, а также данные инструмента инвестирования роботов (см. раздел “Инструмент поддержки принятия инвестиционных решений”) свидетельствует о большом интересе к темам нашего проекта. Ожидается, что для дальнейшего содействия переходу от традиционных производственных парадигм к цифровым и роботизированным средствам, финансируемые ЕС центры цифровых инноваций обеспечат более легкий доступ к технологическим средствам, экспертным знаниям и образовательная и деловая поддержка.