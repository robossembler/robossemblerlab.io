;; Modified domain taken from
;; "Knowledge transfer in robot manipulation tasks" by Jacob O. Huckaby 2014 
(define (domain assembly_domain_hackaby)
	(:requirements :strips :typing :adl :fluents)
	(:types
		arm ; robotic manipulator arm
		workspace ; workspace for assembly operations and storage
		grip weld scan - tool ; manipulator tool for grip, weld, solder and other operations
		part ; solid part, that cannot be disassembled
		assembly ; assembly - can dissassembled
	)

	(:predicates
		(arm-canreach ?a - arm ?s - workspace)
		(arm-at ?a - arm ?s - workspace)
		(arm-capabilities ?a - arm ?t - tool)
		(arm-active ?a - arm ?t - tool)
		(arm-holding ?a - arm ?p - part)
		(part-at ?p - part ?s - workspace)
		(part-state ?p - part ?t - tool)
		(pose-detected ?a - arm ?p - part ?s - workspace)
	)

	(:action activate
		:parameters (?a - arm ?old - tool ?new - tool)
		:precondition (and
			(arm-capabilities ?a ?new)
			(arm-active ?a ?old))
		:effect (and
			(arm-active ?a ?new)
			(not (arm-active ?a ?old))) )
	
	(:action detect-pose-part
		:parameters (?a - arm ?p - part ?s - workspace ?t - scan)
		:precondition (and
			(arm-at ?a ?s)
			(arm-active ?a ?t)
			(part-at ?p ?s))
		:effect (pose-detected ?a ?p ?s)
	)

	(:action detect-pose-workspace
		:parameters (?a - arm ?p - part ?s - workspace)
		:precondition (and
			(arm-at ?a ?s)
			(arm-active ?a detect)
			(arm-holding ?a ?p))
		:effect (pose-detected ?a ?p ?s)
	)
	
	(:action grasp
		:parameters (?a - arm ?p - part ?s - workspace)
		:precondition (and
			(arm-at ?a ?s)
			(arm-active ?a grip)
			(arm-holding ?a no-part)
			(part-at ?p ?s)
			(pose-detected ?a ?p ?s))
		:effect (and
			(arm-holding ?a ?p)
			(not (arm-holding ?a no-part))
			(not (part-at ?p ?s))
			(not (pose-detected ?a ?p ?s)))
	)
	
	(:action ungrasp
		:parameters (?a - arm ?p - part ?s - workspace)
		:precondition (and
			(arm-at ?a ?s)
			(arm-active ?a grip)
			(arm-holding ?a ?p)
			(pose-detected ?a ?p ?s))
		:effect (and
			(arm-holding ?a no-part)
			(not (arm-holding ?a ?p))
			(part-at ?p ?s)
			(not (pose-detected ?a ?p ?s)))
	)
	
	(:action move
		:parameters (?a - arm ?from - workspace ?to - workspace)
		:precondition (and
			(arm-at ?a ?from)
			(arm-canreach ?a ?to)
			(arm-holding ?a no-part))
		:effect (and
			(arm-at ?a ?to)
			(not (arm-at ?a ?from)))
	)
	
	(:action carry
		:parameters (?a - arm ?p - part ?from - workspace ?to - workspace)
		:precondition (and
			(arm-at ?a ?from)
			(arm-canreach ?a ?to)
			(arm-holding ?a ?p))
		:effect (and
			(arm-at ?a ?to)
			(not (arm-at ?a ?from)))
	)
	
	(:action employ
		:parameters (?a - arm ?t - tool ?p - part ?s - workspace)
		:precondition (and
			(arm-at ?a ?s)
			(arm-active ?a ?t)
			(arm-holding ?a no-part)
			(part-at ?p ?s)
			(pose-detected ?a ?p ?s))
		:effect (and
			(part-state ?p ?t)
			(not (pose-detected ?a ?p ?s)))
	)
)