(define (problem p1) (:domain assembly_domain_hackaby)
(:objects
	arm1 arm2 - arm
	gearbox - part
	part-bin workspace - workspace
	grip - tool )
(:init
	(arm-canreach arm1 part-bin)
	(arm-canreach arm1 workspace)
	(arm-canreach arm2 part-bin)
	(arm-canreach arm2 workspace)
	(arm-at arm1 part-bin)
	(arm-at arm2 workspace)
	(arm-capabilities arm1 grip)
	(arm-capabilities arm1 detect)
	(arm-capabilities arm2 grip)
	(arm-capabilities arm2 detect)
	(arm-active arm1 grip)
	(arm-active arm2 grip)
	(arm-holding arm1 no-part)
	(arm-holding arm2 no-part)
	(part-at gearbox part-bin))
(:goal
	(and (part-state gearbox glue)
	(part-state gearbox weld)
	(part-at gearbox assembly-line)))
)