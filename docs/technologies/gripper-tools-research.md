---
id: gripper-tools-research
title: 'Устройства захвата'
---

## Популярные модели захватов

![grippers](https://github.com/columbia-ai-robotics/adagrasp/blob/main/figures/teaser.png?raw=true)

* Robotiq
* Franka Hand
* EZGripper
* Kinova KG-3
* Sawyer
* RG2
* WSG 50
* Barrett Hand-B
* Barrett Hand
* Robotiq 2F-85
* [xArm Gripper](https://www.ufactory.cc/products/xarm-gripper-2020) 

## Robotiq

[Robotiq Tools 3D-models](https://configurator.suite.robotiq.com/)

[ROS-пакеты](https://github.com/ros-industrial/robotiq) для Robotiq-приспособлений. Репозиторий не поддерживается.

[Другая реализация ROS-пакетов](https://github.com/Danfoa/robotiq_2finger_grippers). Лучше описана, чем предыдущий вариант.

[И ещё реализация от компании AcutronicRobotics](https://github.com/AcutronicRobotics/robotiq_modular_gripper). Компаний больше не существует, но оставила [много исходников](https://github.com/AcutronicRobotics) к своему роботу-манипулятору [MARA](https://github.com/AcutronicRobotics/MARA). Исходники заархивированы более 2-х лет назад.

Исходники захвата Robotiq закрыты, но сами макеты могут быть использованы для интеграции с другими захватами. Либо взяты за образец

## Nyrio

[Три варианта грипперов](https://github.com/NiryoRobotics/niryo_one/tree/master/STL/7-Tools) в формате отдельных деталей STL

## Open Source трёх-пальцевый захват от Astana Laboratory of Robotics and Intelligent Systems

[Исходники](https://www.alaris.kz/research/open-source-3d-printed-underactuated-robotic-gripper/)(в конце статьи) | [Статья](https://www.alaris.kz/wp-content/uploads/2014/10/MESA14_114.pdf)

## Robot Gripper Mechanism

[Видео + модель в STEP](https://howtomechatronics.com/miscellaneous/3d-models/robot-gripper-mechanism/)

## RG2

https://onrobot.com/ru/izdeliya/zakhvatnoe-ustroystvo-rg2

[Datasheet](https://onrobot.com/sites/default/files/documents/Datasheet_RG2_v1.3_EN.pdf?_gl=1*1agde0z*_ga*Njc0MTk3Njc5LjE2MjUzMjU1MzA.*_up*MQ..)
