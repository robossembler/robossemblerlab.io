---
id: wood
title: Применение древесины
---

В развитии _самовоспроизводящихся автоматизированных систем(САС)_ есть одно немаловажное обстоятельство: прежде чем будет создан даже первый и весьма несовершенный образец полностью воспроизводимого САС, потребуется пройти многочисленные промежуточные стадии для отработки всей цепочки технологии, задействованной в САС, применительно к сырью и источникам энергии, а также поддерживать связь со сбытом, то есть производить некий продукт, пригодный для продажи. Вторая задача не менее важна, чем первая, обуславливает масштаб работ и весь успех проекта.

Разумеется, промежуточные стадии и продукт надо подбирать наиболее близкими, чтобы не уходить в побочные технологические ответвления. 

В настоящих заметках я пишу только о принципиальных решениях, почти без расчетов технологического или экономического свойства, поскольку необходимые данные можно получить лишь путем опытов с автоматическим или полуавтоматизированным оборудованием. 

## Измельченная древесина

В северных природно-географических условиях наиболее доступным сырьем для САС является древесина или более шире древесно-растительное сырье, включая однолетние и многолетние растения с развитым стеблем (такие как крапива, борщевик Сосновского и т.д.).

Древесное сырье – это не только деловая древесина, но и еще многочисленные виды сырья, ныне воспринимаемые как отходы: валежник, сухостой, различные виды неделовой древесины, вроде тополя или ивы, подрост деревьев и так далее. К этим источникам древесно-растительного сырья можно отнести также различные виды быстрорастущих видов, которые могут быть интродуцированы. 

Существует интересный и перспективный способ изготовления древесных изделий из древесной пульпы, смешанной с ПВА (пропорция: 80% древесной пульпы и 20% ПВА с добавлением растительных масел в качестве пластификатора). Этот состав отлично формуется, прессуется, может выдавливаться экструзией. После застывания становится аналогичен древесине, пригоден для мехобработки, полировки, покрытия лаком и т.д. Подготовленный состав может храниться в упаковке до 1 года, а потом может быть регенерирован с помощью ацетона. 

Основной компонент состава – перемолотая до пылеобразного состояния древесина (частицы менее 1 мм). Получение ее не вызывает трудностей, поскольку имеется большое количество измельчительных машин для получения щепы или более мелких частиц древесины. 

Измельченная древесина представляет собой, таким образом, хороший продукт, который может быть использован как для нужд САС, так и в качестве товарного продукта. 

## Для нужд САС

Многие части роботов могут изготовляться из дерева, для чего в особенности подходит пульпа древесины и ПВА, позволяющая штамповать или отливать детали любой сложности. Правда, изделия из подобной пульпы отличаются сравнительно невысокой термостойкостью (от – 10 до +40 градусов). Для изделий с повышенными требованиями можно использовать или эпоксидные смолы или фурановые клеи. Последние могут образовывать твердые смолы очень высокой термостойкости, порядка 1200 градусов С. 
В качестве товарного продукта
Измельченная древесина может быть товарным продуктом, выступая как полуфабрикат для производства следующих продуктов:
- древесная пульпа на основе ПВА для декоративных работ (качество древесного сырья не важно, требуется упаковка, например, в пластиковые тубы);
- для производства бумаги и для лесохимической переработки (для разных нужд требуется выполнение стандартов: соотношение пород древесины, влажность, размеры гранул и т.д.)
- топливные древесные пеллеты (качество древесного сырья не важно, требуется определенная влажность). 

Это основные сферы применения измельченной древесины, в которые автономные комплексы могут встроиться поставщиками полуфабриката, заготавливая сырье и обрабатывая его в отдаленной, малонаселенной местности. Комплекс роботов заготавливает древесное сырье, измельчает его, подсушивает, складирует в хранилище, откуда производится отгрузка на транспорт.

Для заготовки измельченной древесины комплекс роботов состоит из подвижных машин, которые могут передвигаться по определенной территории, станции с оборудованием по измельчению, сушке и хранению продукции с обслуживающими роботами, а также энергетической установки и охранных устройств.

Если ставится задача заготовки измельченной древесины, то подвижные машины по поиску и сбору сырья могут оснащаться режущими устройствами, которые откусывают или отрезают от дерева куски древесины, которые потом измельчаются (подобно бобру; возможно режущее устройство будет аналогично челюсти бобра), либо же режет растение с древесным стеблем на куски на корню. Отрезанные куски подвижная машина складирует в кузов-накопитель, а потом отвозит на станцию.

Заготовка измельченной древесины должна ориентироваться на отходы, такие как сухостой, валежник, хворост, упавшие шишки и т.д., то есть такой комплекс может выполнять работы по очистке леса или вырубки от отходов. Выполнение таких работ может быть отдельным источником средств. Также комплекс может обслуживать плантацию быстрорастущих деревьев или перерабатывать зеленый мусор. 
Это наиболее универсальный способ разработки древесно-растительного сырья автономными автоматическими системами. 

## Клееные древесные материалы

Существует технология производства клееных досок, брусов и щитов из отдельных брусков или малоразмерных досок (ламелей; в том числе составных и скрепленных микрошипами), вырезаемых из малоразмерного пиломатериала или лесопильных отходов. Эта технология хорошо отработана и позволяет получать материалы высокого качества из низкокачественного исходного сырья.

Это тоже интересное направление для автономных автоматических систем, причем для собственных нужд и в качестве товарной продукции. 
Для нужд САС: брусья, доски, плиты могут использоваться в качестве строительного материала для сооружения разного рода ангаров, помещений для персонала, ограждений, а также в качестве материала для сооружения плотин микро-ГЭС, элементов энергетических установок, опор для ЛЭП и т.п. целей инфраструктурного освоения территории.

В качестве товарного продукта: брусья, доски и плиты (при условии соответствия их стандартам). Также возможно разработать новые виды продукции этой категории. Автоматы могут вырезать из древесины тонкие элементы (размером с карандаш или деревянную линейку) и выклеивать из них более прочные или более гибкие материалы, чем обычно. Возможно выклеивание плит или брусьев с пространственной структурой для придания дополнительной прочности. При выстругивании длинного ленточного шпона можно также выклеивать плиты с перекрестной структурой, но более прочные, чем имеющиеся в производстве. При прокрашивании элементов красителями, возможно выклеивать заготовки для инкрустации – декоративные панели. Изготовление инкрустаций весьма сложно и трудоемко, но может быть автоматизировано.

Полная автоматизация заготовки полуфабрикатов, а также изготовления клееных древесных материалов при первом подходе выглядит экономически перспективной. 

## Химическая обработка древесины

Весьма интересным представляется также химическая переработка древесины с получением различных продуктов. Стоит отметить, что некоторые полуфабрикаты, получаемые при химической переработке, предусматривают весьма длинные технологические цепочки и производство целого ряда готовых продуктов или даже товаров. В этих заметках я лишь указываю на возможность этого, но не провожу детального планирования технологических цепочек и подробных расчетов. Продукты могут использоваться для нужд САС, а также для отпуска в качестве товарной продукции.

___Бумага.___ Из проваренной в щелоке и раздробленной массы древесных волокон (лучше подходят многолетние травы с древесным стеблем, лесной подрост, ветки, солома и т.п. виды древесного сырья) нетрудно изготовить бумагу по классическому способу формирования листа бумаги на мелкой сетки. Хотя этот способ изначально ручной, тем не менее, его можно адаптировать к автоматизации. Щелок изготовляется из древесной золы. Производство бумаги по этому способу не требует доставки химикатов и полностью автономно. 
Фурфурол и смолы. Фурфурол может быть получен при кипячении древесной массы в серной кислоте (фурфурол при этом отгоняется с водяным паром). Из тонны древесины можно получить порядка 180-220 кг фурфурола. Фурфурол и фурфуриловый спирт используются для производства весьма прочных и плотных (до 1,2 гр/куб см) смол, которые отверждаются при нагревании. Отвержденные смолы представляют собой плотные и прочные пластмассы.

___Этанол.___ При производстве фурфурола образуется побочный раствор сахаров, который, после гашения кислоты (обычно известью или известковым молоком) пригоден для сбраживания. Впрочем, мои эксперименты показали, что не так трудно добиться весьма интенсивного гидролиза целлюлозы любого сырья (древесина, хлопок, тополиный пух и т.п.), без кипячения в кислоте и вообще без затрат энергии. Производство этанола может быть весьма крупномасштабным, побочные продукты могут быть переработаны термическим разложением в мазутоподобный продукт. Дрожжи, остающиеся после брожения, могут быть использованы как сырье для пиролиза, как корма или как пищевой продукт (последнее, впрочем, зависит от способа очистки).

Однако, для автоматических систем по переработке древесины, фурфурольно-этаноловый цикл более предпочтительный, потому что из сырья извлекается сразу два продукта.

Можно выделить три цепочки переработки этанола в различные продукты. 
1-я цепочка, _из этилового спирта_ – бутадиен и бутадиеновый каучук (это особенно важно для САС), этилнитрат и диэтиловый эфир. Также этилен – родоначальник 2-й цепочки. 
2-я цепочка, _из этилена_ – полиэтилен, хлорэтан с последующей переработкой в стирол и полистирол, а также окись этилена – родоначальник 3-й цепочки
3-я цепочка, _из окиси этилена_ – акрилонитрил и далее акрил – искусственное текстильное волокно, этиленгликоль и его полимер полиэтиленгликоль – смазочные вещества, антифризы, а также сырье для получения полиэтилентерефталата.

Это был результат только весьма поверхностного исследования, и нельзя исключать, что возможны другие технологические цепочки и продукты. Для САС и автоматических систем лесопереработки с этанольным циклом, наиболее важны 1-я и 3-я цепочка, то есть получение бутадиенового каучука (изоляция, амортизаторы, элементы ходовой части), и полиэтиленгликоля в качестве смазочного вещества. Часть этанола может использоваться в качестве моторного топлива.

___Сухая перегонка древесины.___ При термическом разложении древесины в диапазоне температур от 600 до 900 градусов получается ряд продуктов: уксусная кислота, метанол, ацетон, и древесный уголь. Древесный уголь довольно активно продается в качестве топлива, а также может быть использован для плавки металлов.

Интересно, что поливинилацетат может быть произведен автоматической системой. Винилацетат образуется из ацетилена и уксусной кислоты при нагревании до 220 градусов, в присутствии ацетата цинка на активированном угле. Из винилацетата можно получить ПВА – компонент для древесной пульпы, описанной выше. Некоторые трудности представляет собой получение ацетилена, однако есть способ получения его в дуговом разряде между угольными электродами в атмосфере водорода. Угольные электроды – из древесного угля; водород можно получить в составе синтез-газа, при реакции водяного пара с раскаленным углем, а затем отделить его от угарного газа; электроэнергия может быть получена паровой установкой, установкой на основе двигателя Стирлинга или от небольшой ГЭС.

Как видим, спектр продукции, которую можно получить из древесного сырья, довольно большой и, вероятно, еще может быть расширен. Но пока целесообразнее остановиться на этих вариантах. 

## Архитектура автономной системы, базирующейся на древесном сырье

Все это хорошо, но весь вопрос в том, как это создать? И с чего начать? 
Начинать следует с элемента, без которого система не существует и функционировать не может. В нашем случае это подвижной робот для сбора древесного сырья.

Это должен быть робот на шасси высокой проходимости, с достаточным запасом хода, чтобы двигаться по некоторому маршруту и перевозить собранный груз. Он также должен быть оснащен манипуляторами, на которых установлены режуще-захватывающие устройства, которыми робот отрезает часть дерева, ветки, стебля, захватывает и переносит отрезок в накопительный бункер, установленный на роботе.

Требуется система автономной навигации (на основе GPS или цифровой карты со счислением пройденного маршрута), а также средства распознавания древесного сырья. Сейчас уже есть программы распознавания видов растений по фотоснимку, так что камер и соответствующих программ достаточно для решения этой задачи. Робот может различать породы деревьев и заготавливать лишь нужные в данный момент. Также робот с помощью камеры и программы может оценивать размеры обнаруженного древесного сырья и намечать план его разрезания на куски. 
Должен ли робот быть ориентирован лишь на безморозный период или он должен быть также адаптирован к зимним условиям - это вопрос пока дискуссионный. С одной стороны зима – наиболее удобное время для заготовки древесного сырья, поскольку увеличивается несущая способность переувлажненных грунтов и доступность лесов; с другой стороны – снежный покров будет препятствовать движению робота, а также затруднять поиск и разрезание древесного сырья.

Ввиду большой площади болот и лесов с переувлажненным грунтом, в которых лесозаготовители работать не могут и эти лесные ресурсы оказываются вне использования, целесообразнее адаптировать робота к этим условиям (шнекоход или другая амфибия). Шнекоходный робот также может активно использоваться зимой.

Минимальный пусковой комплекс: 2-3 робота, ангар для их хранения и ремонта, заправочная станция, а также склад. Такой комплекс уже позволяет получать товарную древесную щепу.

Энергетический узел должен обеспечить все потребности в топливе и энергии. Потому даже в минимальном варианте он должен включать в себя хранилище моторного топлива (если роботы оснащаются ДВС), электрогенератор (на основе ДВС, микро-ГЭС – что предпочтительно, иногда ветровые установки, особенно роторного типа), а также отопительное устройство на основе пиролизной печи, поскольку ангар и склад нуждаются в отоплении, а также требуется тепловая энергия для подсушивания сырья. Зола после сгорания древесины в пиролизной печи используется для получения щелока.

Следующий этап развития комплекса состоит в создании перерабатывающего комплекса. Склад сырья выступает в качестве входного узла в этот комплекс. Здесь возможны различные варианты. Например, создается комплекс по переработке древесного сырья в древесную пульпу с ПВА. Для этого нужен участок измельчения древесины, участок смешивания древесной пыли с ПВА, подготовки смеси, а также участок упаковки пульпы или формовки изделий, затем склад готовой продукции.

Конкретное решение зависит от архитектуры комплекса, но в целом это будут ангары свободной планировки. Транспортные связи между участками целесообразнее всего организовать с помощью простых транспортных роботов (шасси, кузов, система навигации и стыковки с погрузочно-разгрузочным узлами, вспомогательный манипулятор; питание – электрическое от аккумуляторов). Внутри ангара должны быть проложены дорожки для транспортных роботов.

Чистку и уборку ангара, чистку и обслуживание оборудования также нужно запроектировать автоматической, с помощью транспортных роботов, которые подносят и подключают шланги систем обслуживания: пылесоса, паровой установки, моющей установки. Но стоит сказать, что этот вопрос пока еще требует изучения и обдумывания.

Автономная автоматическая система развивается по пути расширения и усложнения перерабатывающего комплекса. Один ангар – переработка в древесную пульпу, другой ангар – переработка в бумагу, третий ангар – гидролизная, сбраживание и производство этанола и так далее. Соответственно расширяются мощности склада сырья и увеличивается парк подвижных роботов (но до определенных пределов, чтобы не возникли излишние расходы топлива и энергии на сбор древесного сырья), а также увеличивается мощность энергетического узла. Для распределения сырья, материалов, энергоресурсов, а также общего контроля за процессами потребуется создание управляющего центра, на основе компьютера, оснащенного различными датчиками, камерами и устройствами связи с роботами

Создается интегрированная система водоснабжения и очистки вод, хранилища или промежуточные склады для отходов. В энергетическом центре целесообразно вводить установку по термическому разложению органики (древесных остатков, дрожжей) и получению горючего газа и жидкого топлива для энергетических или химических нужд, а также дистилляционную установку для очистки сточных вод с содержанием органики, кислот, щелочи и т.п. веществ.

Возможно, что будет целесообразно создать промежуточный тепловой центр, который объединяет производства, в котором тепло вырабатывается (например, пиролиз, сжигание и т.д.) с теми производствами, где тепло потребляется (перегонка бражки, дистилляция воды, сушка и т.д.).

Транспорт твердых веществ и готовой продукции – с помощью транспортных роботов; транспорт жидкостей (воды, растворов кислот и щелочи и т.д.) с помощью трубопроводов, оборудованных приемными и выпускными емкостями и насосами; газов – с помощью газопроводов, также оборудованных приемными и выпускными емкостями и газовыми насосами.

Это описание автоматической системы в самых общих чертах. Даже в наиболее простых ее разновидностях это получается довольно сложная система, состоящая из многих подсистем; очень наукоемкая и требующая большой изобретательской, проектной и опытно-конструкторской работы. Многие узлы и системы потребуют предварительного конструирования и испытания в лабораторных условиях. Например, для сухой перегонки древесины потребуется создать автоматическое перегонное устройство с точным контролем температуры и режима перегонки, чтобы можно было разделить фракцию без промежуточных стадий.

Системы контроля доступа и безопасности пока что не рассматриваются. 

## Ближайшие задачи и перспективы

В качестве ближайших задач целесообразно поставить цели достижения производства технологической щепы (смешанной и по ТУ), затем древесной пульпы с ПВА, затем упаковочной бумаги. Эти три продукта пригодны для продажи в качестве товара. 
Возможно добиться преференций и определенной поддержки, если ориентировать такие системы на очистку лесов и вырубок от горючих древесных отходов; эти системы могут применяться во многих регионах и весьма широко. 
Все остальное требует предварительного этапа опытов, конструирования и испытания, а также интеграции отдельных установок и устройств в единую и работоспособную систему.

_Верхотуров Д.В._