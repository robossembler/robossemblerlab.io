---
slug: robossembler-is-started
title: Проект Робосборщик начат
author: Игорь Брылёв
author_title: Team Lead @ Robossembler
author_url: https://github.com/movefasta
author_image_url: https://avatars.githubusercontent.com/u/5147021?s=60&v=4
tags: [robossembler, start]
---

Проект Робосборщик начат